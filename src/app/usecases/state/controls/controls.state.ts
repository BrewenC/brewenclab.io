import { Theme } from '@domain/constants/global.const';

export const ControlsFeatureKey = 'controls';

export interface ControlsState {
  theme: Theme;
  navigationCollapsed: boolean;
}

export const ControlsInitialState: Readonly<ControlsState> = {
  theme: window.matchMedia('(prefers-color-scheme: dark)').matches
    ? Theme.Dark
    : Theme.Light,
  navigationCollapsed: true,
};
