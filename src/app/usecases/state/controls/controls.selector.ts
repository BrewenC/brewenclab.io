import { createFeatureSelector, createSelector } from '@ngrx/store';

import { Theme } from '@domain/constants/global.const';

import { ControlsFeatureKey, ControlsState } from './controls.state';

export const selectControls =
  createFeatureSelector<ControlsState>(ControlsFeatureKey);

export const selectTheme = createSelector(
  selectControls,
  (state: ControlsState): Theme => state.theme,
);

export const selectNavigationCollapsed = createSelector(
  selectControls,
  (state: ControlsState): boolean => state.navigationCollapsed,
);
