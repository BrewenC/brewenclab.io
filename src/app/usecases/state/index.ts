import { ControlsReducer } from './controls/controls.reducer';

export const AppReducers = {
  controls: ControlsReducer,
};
