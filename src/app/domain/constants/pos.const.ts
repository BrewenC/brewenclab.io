export enum PointerType {
  Mouse = 'mouse',
  Touch = 'touch',
}

export type Coordinate = {
  x: number;
  y: number;
};
export type TypedCoordinate = {
  cursor: PointerType;
} & Coordinate;

export type Distance = {
  x: number;
  y: number;
};
export type TypedDistance = {
  cursor: PointerType;
} & Distance;
