import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { faBars } from '@fortawesome/free-solid-svg-icons';

import { TITLE } from '@domain/constants/global.const';
import { ControlsActions } from '@usecases/state/controls/controls.actions';

@Component({
  selector: 'topbar',
  templateUrl: './topbar.template.html',
  styleUrls: ['./topbar.style.scss'],
})
export class TopbarComponent {
  protected readonly _title: string = TITLE;
  protected readonly _faBars = faBars;

  constructor(private readonly store: Store) {}

  toggleNavbar() {
    this.store.dispatch(ControlsActions.toggleNavigation({ collapsed: false }));
  }
}
