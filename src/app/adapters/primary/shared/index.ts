import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { TopbarComponent } from './topbar/topbar.component';
import { NavbarComponent } from './navbar/navbar.component';

const Components = [TopbarComponent, NavbarComponent];

@NgModule({
  imports: [FontAwesomeModule, RouterModule],
  declarations: Components,
  exports: Components,
  providers: [
    {
      provide: Window,
      useValue: window,
    },
  ],
})
export class AdaptersPrimarySharedModule {}
