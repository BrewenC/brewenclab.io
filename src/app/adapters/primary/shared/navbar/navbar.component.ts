import { Component, ElementRef, HostListener, ViewChild } from '@angular/core';
import {
  ActivatedRoute,
  NavigationEnd,
  Router,
  RouterState,
  TitleStrategy,
} from '@angular/router';
import { Store } from '@ngrx/store';

import { ConvertUtils } from '@libs/utils/convert.utils';

import { EventBus } from '@usecases/events';
import { GesturesEvent } from '@usecases/events/gestures.event';
import { selectNavigationCollapsed } from '@usecases/state/controls/controls.selector';
import { ControlsActions } from '@usecases/state/controls/controls.actions';

import { Responsive } from '@domain/constants/responsive.const';
import {
  PointerType,
  TypedCoordinate,
  TypedDistance,
} from '@domain/constants/pos.const';

@Component({
  selector: 'navbar',
  templateUrl: './navbar.template.html',
  styleUrls: ['./navbar.style.scss'],
})
export class NavbarComponent {
  isCollapsed = true;
  isMobile = this.window.innerWidth <= Responsive.MD;

  routeTitle: string = 'Unknown';

  constructor(
    private readonly window: Window,
    private readonly router: Router,
    private readonly titleStrategy: TitleStrategy,
    private readonly eventBus: EventBus,
    private readonly store: Store,
  ) {}

  @ViewChild('navbar') navbar!: ElementRef;
  @ViewChild('navbarSide') navbarSide!: ElementRef;

  private _elementWidth: number = 0;
  private _sideWidth: number = 0;
  private _contentWidth: number = 0;

  ngOnInit() {
    this.router.events.subscribe(this.onRouteEvent);

    this.eventBus.subscribe<TypedCoordinate>(
      GesturesEvent.PanStart,
      (coords: TypedCoordinate) => this._isTouch(coords, this.onPanStart),
    );
    this.eventBus.subscribe<TypedCoordinate>(
      GesturesEvent.PanEnd,
      (coords: TypedCoordinate) => this._isTouch(coords, this.onPanEnd),
    );
    this.eventBus.subscribe<TypedDistance>(
      GesturesEvent.PanMove,
      (distance: TypedDistance) => this._isTouch(distance, this.onPanMove),
    );

    this.store.select(selectNavigationCollapsed).subscribe(this.setCollapsed);

    document.addEventListener('click', this.onDOMClick);

    this.onResize();
  }
  ngOnDestroy() {
    document.removeEventListener('click', this.onDOMClick);
  }

  setCollapsed = (collapsed: boolean) => {
    this.isCollapsed = collapsed;
  };

  collapse() {
    this.store.dispatch(ControlsActions.toggleNavigation({ collapsed: true }));
  }

  expand() {
    this.store.dispatch(ControlsActions.toggleNavigation({ collapsed: false }));
  }

  // Listeners

  onRouteEvent = ($event: any) => {
    if ($event instanceof NavigationEnd) {
      this.routeTitle = this._getRouteTitle(
        this.router.routerState,
        this.router.routerState.root,
      ).join(' / ');

      if (this.isMobile) {
        this.collapse();
      }
    }
  };

  onDOMClick = ($event: MouseEvent) => {
    const $target: HTMLElement = $event.target as HTMLElement;

    if (
      this.navbar.nativeElement.contains($target) ||
      $target.classList.contains('menu-btn') ||
      this.isCollapsed
    ) {
      return;
    }

    this.collapse();
  };

  // Gestures
  isPanning = false;

  baseOffset: number = 0;
  panningPercentage: number = 0;
  panningOffset: number = 0;

  private readonly _panningMultiplier = 2;

  get panningTransform() {
    if (!this.isPanning) {
      return '';
    }

    return this.isPanning ? `translateX(${this.panningOffset}px)` : '';
  }
  get backdropOpacity() {
    if (!this.isPanning) {
      return this.isCollapsed ? 0 : 1;
    }

    return 1 - this.panningPercentage;
  }

  onPanStart = () => {
    this.baseOffset = this.isCollapsed ? this._contentWidth : 0;
    this.panningOffset = this.baseOffset;

    this.isPanning = true;
  };
  onPanEnd = () => {
    this.isPanning = false;
    this.panningOffset = 0;
  };
  onPanMove = (distance: TypedDistance) => {
    const percentage = distance.x / this.window.innerWidth;
    const offset = percentage * this._contentWidth * this._panningMultiplier;
    const fullOffset = this.baseOffset + offset;

    if (fullOffset < 0) {
      this.panningOffset = 0;
    } else if (fullOffset > this._contentWidth) {
      this.panningOffset = this._contentWidth;
    } else {
      this.panningOffset = fullOffset;
    }

    const visiblePercentage = this.panningOffset / this._contentWidth;
    if (visiblePercentage > 0.5) {
      this.collapse();
    } else {
      this.expand();
    }

    this.panningPercentage = visiblePercentage;
  };

  @HostListener('window:resize')
  onResize = () => {
    const width = this.window.innerWidth;

    this._elementWidth = ConvertUtils.remToPx(10);
    this._sideWidth = width > Responsive.MD ? ConvertUtils.remToPx(3) : 0;
    this._contentWidth = this._elementWidth - this._sideWidth;

    this.isMobile = width <= Responsive.MD;
  };

  // Helpers

  private _getRouteTitle(state: RouterState, parent: ActivatedRoute): string[] {
    const data = [];

    if (
      parent &&
      parent.snapshot &&
      this.titleStrategy.getResolvedTitleForRoute(parent.snapshot)
    ) {
      data.push(this.titleStrategy.getResolvedTitleForRoute(parent.snapshot));
    }
    if (state && parent && parent.firstChild) {
      data.push(...this._getRouteTitle(state, parent.firstChild));
    }
    return data ?? ['Unknown'];
  }

  private _isTouch = (
    payload: TypedCoordinate | TypedDistance,
    callback: CallableFunction,
  ) => {
    if (payload.cursor === PointerType.Touch) {
      callback(payload);
    }
  };
}
