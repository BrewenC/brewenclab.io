import { Component } from '@angular/core';

@Component({
  selector: 'view-404',
  templateUrl: './not-found.template.html',
  styleUrls: ['./not-found.style.scss'],
})
export class NotFoundComponent {}
