# Brewen.dev - Front
#### Deployment
* Production: [brewen.dev](https://www.brewen.dev)
* Preview: [dev.brewen.dev](https://dev.brewen.dev)

[![brewen.dev][website-badge]][website-url]
[![node][node-badge]][node-url]
[![package][package-badge]][package-url]
[![license][license-badge]][license-url]

### Setup
##### Development
```bash
# Install dependencies
yarn install
# Start the server
yarn start
```
##### Production
```bash
# Install dependencies
yarn install --freeze-lockfile
# Build the server
yarn build
# Serve it on a static host provider (pages, vercel...)
```
###### ⚠️ Hash routing needs to be enabled for hosts like GitLab or GitHub pages ([src/configs/app.routes.ts](src/configs/app.routes.ts))

<hr/>
<p align="right">
  <a href="LICENSE">GNU GPLv3</a> &copy; 2023 Brewen Couaran
</p>



[website-badge]: https://img.shields.io/website?url=https%3A%2F%2Fwww.brewen.dev&label=brewen.dev
[website-url]: https://www.brewen.dev/
[node-badge]: https://img.shields.io/badge/dynamic/json?url=https%3A%2F%2Fgitlab.com%2Fbrewen.dev%2Ffront%2F-%2Fraw%2Fmaster%2Fpackage.json%3Fref_type%3Dheads&query=%24.node&label=node&color=%23339933
[node-url]: https://nodejs.org/en/
[package-badge]: https://img.shields.io/badge/dynamic/json?url=https%3A%2F%2Fgitlab.com%2Fbrewen.dev%2Ffront%2F-%2Fraw%2Fmaster%2Fpackage.json&query=%24.packageManager&label=package
[package-url]: https://yarnpkg.com/
[license-badge]: https://img.shields.io/gitlab/license/brewen.dev%2Ffront
[license-url]: https://www.gnu.org/licenses/gpl-3.0.en.html

